package latihan6restapi.service;

import latihan6restapi.entity.Karyawan;

import java.util.Map;

public interface KaryawanServic {
    public Map getAll();

    public Map getDetail(Long karyawanId);

    public Map insert(Karyawan karyawan);

    public Map update(Karyawan karyawan);
}
