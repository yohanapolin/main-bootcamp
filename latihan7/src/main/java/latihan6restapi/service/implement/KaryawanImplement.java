package latihan6restapi.service.implement;

import latihan6restapi.entity.Karyawan;
import latihan6restapi.repository.KaryawanRepository;
import latihan6restapi.service.KaryawanServic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
@Transactional
public class KaryawanImplement implements KaryawanServic {

    @Autowired
    public KaryawanRepository karyawanRepository;

    @Override
    public Map getAll() {
        List<Karyawan> list = new ArrayList<Karyawan>();
        Map<String, Object> map = new HashMap<>();
        try {
            list = karyawanRepository.getList();
            map.put("data", list);
            map.put("status code", 200);
            map.put("message", "Get all karyawan");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status code", 500);
            map.put("error", e);
            map.put("message", "Cant get all karyawan ");
            return map;
        }
    }

    @Override
    public Map getDetail(Long karyawanId) {
        Map<String, Object> map = new HashMap<>();
        try {
            Karyawan obj = karyawanRepository.getbyID(karyawanId);
            map.put("data", obj);
            map.put("status", 200);
            map.put("message", "Get karyawan data");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status code", 500);
            map.put("error", e);
            map.put("message", "Error get karyawan");
            return map;
        }
    }

    @Override
    public Map insert(Karyawan karyawan) {
        Map<String, java.io.Serializable> map = new HashMap<>();
        try {
            Karyawan obj = karyawanRepository.save(karyawan);
            map.put("data", obj);
            map.put("status code", 201);
            map.put("message", "Insert karyawan success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error insert karyawan");
            return map;
        }
    }

    @Override
    public Map update(Karyawan karyawan) {
        Map<String, java.io.Serializable> map = new HashMap<String, java.io.Serializable>();
        try {
            Karyawan obj = karyawanRepository.getbyID(karyawan.getId());
            if (karyawan.getNama() != null &&
                    karyawan.getNama().length() > 0 &&
                    !Objects.equals(obj.getNama(), karyawan.getNama())) {
                obj.setNama(karyawan.getNama());
            }
            obj.setJeniskelamin(karyawan.getJeniskelamin());
            obj.setDob(karyawan.getDob());
            obj.setAlamat(karyawan.getAlamat());
            obj.setStatus(karyawan.getStatus());
            karyawanRepository.save(obj);
            map.put("data", obj);
            map.put("status code", 200);
            map.put("message", "Update karyawan success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status code", 500);
            map.put("error", e);
            map.put("message", "Error update karyawan");
            return map;
        }
    }

}
