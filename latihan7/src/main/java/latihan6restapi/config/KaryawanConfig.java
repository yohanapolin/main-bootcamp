package latihan6restapi.config;

import latihan6restapi.entity.Karyawan;
import latihan6restapi.repository.KaryawanRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;

@Configuration
public class KaryawanConfig {

    @Bean
    CommandLineRunner commandLineRunner(
            KaryawanRepository repository) {
        return args -> {
            Karyawan satu =new Karyawan(

                    "Natasyah",
                    0,
                    LocalDate.of(2000, Month.APRIL, 1),
                    "medan",
                    0
            );
            Karyawan dua = new Karyawan(

                    "Oktaf",
                    1,
                    LocalDate.of(1999, Month.APRIL, 20),
                    "medan",
                    1
            );
            Karyawan tiga = new Karyawan(

                    "Samar",
                    0,
                    LocalDate.of(1990, Month.AUGUST, 3),
                    "medan",
                    1
            );

            repository.saveAll(
                    Arrays.asList(satu,dua , tiga)
            );
        };
    }
}
