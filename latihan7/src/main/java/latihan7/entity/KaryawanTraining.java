package latihan7.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "karyawan_training")
public class KaryawanTraining {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "karyawan_training_sequence"
    )
    private Long id;

    @ManyToOne
    @JoinColumn(name = "training_id")
    private Training training;
    private Date date_of_training;

    @ManyToOne
    @JoinColumn(name = "karyawan_id")
    private Karyawan karyawan;
}
