package latihan7.entity;


import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table( name ="karyawan_detail")
public class KaryawanDetail implements Serializable {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "karyawan_detail_sequence"
    )
    private  Long id;
    private String nik;
    private String npwp;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "karyawan_id")
    private Karyawan karyawan;


}
