package latihan7.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import net.bytebuddy.dynamic.loading.InjectionClassLoader;
import org.springframework.boot.autoconfigure.web.WebProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Data
@Table(name ="karyawan")
public class Karyawan implements Serializable {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "karyawan_sequence"

    )
    private Long id;
    private String name;
    private String gender;
    private Date dob;
    private String address;
    private Integer status;

    @OneToOne(mappedBy = "karyawan")
    private KaryawanDetail karyawanDetail;

    @JsonIgnore
    @OneToMany(mappedBy = "karyawan",  fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Rekening> rekenings;


    @OneToMany(mappedBy = "karyawan")
    Set<KaryawanTraining> karyawanTrainings;



}
