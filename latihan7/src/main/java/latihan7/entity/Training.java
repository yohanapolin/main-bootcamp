package latihan7.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Data
@Entity
@Table(name = "training")
public class Training implements Serializable {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "training_sequence"
    )
    private Long id;
    private String thema;
    private String mentor;

    @OneToMany(mappedBy = "training")
    Set<KaryawanTraining> karyawanTrainings;

}
