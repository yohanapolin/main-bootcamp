package latihan7.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "rekening")
public class Rekening implements Serializable {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "rekening_sequence"
    )
    private  Long id;
    private String name;
    private String type;
    private String nomor;

    @ManyToOne
    @JoinColumn(name = "karyawan_id")
    private Karyawan karyawan;


}
