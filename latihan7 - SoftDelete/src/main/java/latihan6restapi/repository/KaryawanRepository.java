package latihan6restapi.repository;

import latihan6restapi.entity.Karyawan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface KaryawanRepository extends PagingAndSortingRepository<Karyawan, Long> {
    @Query("select k from Karyawan k")
    public List<Karyawan> getList();

    @Query("select k from Karyawan k WHERE k.id = :id")
    public Karyawan getbyID(@Param("id") Long id);


}
