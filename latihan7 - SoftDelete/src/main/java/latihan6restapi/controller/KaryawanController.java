package latihan6restapi.controller;

import latihan6restapi.entity.Karyawan;
import latihan6restapi.repository.KaryawanRepository;
import latihan6restapi.service.KaryawanServic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/karyawan")
public class KaryawanController {

    private final KaryawanServic karyawanServic;

    @Autowired
    public KaryawanController(KaryawanServic karyawanServic) {
        this.karyawanServic = karyawanServic;
    }

    @Autowired
    public KaryawanRepository karyawanRepository;

    @GetMapping
    public Map getAllKaryawan() {
        return karyawanServic.getAll();
    }

    @GetMapping(path = "{Id}")
    public Map getDetailKaryawan(@PathVariable("Id") Long karyawanId) {
        return karyawanServic.getDetail(karyawanId);
    }

    @PostMapping
    public Map postKaryawan(@Valid @RequestBody Karyawan karyawan) {
        return karyawanServic.insert(karyawan);
    }

    @PutMapping
    public Map updateKaryawan(@Valid @RequestBody Karyawan karyawan) {
        return karyawanServic.update(karyawan);
    }

}
