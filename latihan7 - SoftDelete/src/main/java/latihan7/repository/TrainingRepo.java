package latihan7.repository;

import latihan7.entity.Karyawan;
import latihan7.entity.Rekening;
import latihan7.entity.Training;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrainingRepo extends PagingAndSortingRepository<Training, Long> {
    @Query("SELECT  t from  Training  t")
    public List<Training> getList();

    @Query("select t from Training  t WHERE t.id = :id")
    public Training getByID(@Param("id") Long id);

    Page<Training> findByThema(String thema, String mentor, Pageable pageable);
    Page<Training> findByMentor(String mentor, Pageable pageable);


}
