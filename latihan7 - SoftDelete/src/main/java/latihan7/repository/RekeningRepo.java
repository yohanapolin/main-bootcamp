package latihan7.repository;


import latihan7.entity.Rekening;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface RekeningRepo extends PagingAndSortingRepository<Rekening, Long> {

    @Query("select r from Rekening r ")
    public List<Rekening> getList();


    @Query("select r from Rekening r where r.id = :id")
    public Rekening getByID(@Param("id") Long id);

    Page<Rekening> findByType(String type, Pageable pageable);
}
