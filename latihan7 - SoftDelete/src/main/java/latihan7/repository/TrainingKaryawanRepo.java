package latihan7.repository;

import latihan7.entity.Karyawan;
import latihan7.entity.KaryawanTraining;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrainingKaryawanRepo extends PagingAndSortingRepository<KaryawanTraining, Long> {

        @Query("select kt from KaryawanTraining kt")
        public List<KaryawanTraining> getList();

    @Query("select kt from KaryawanTraining  kt WHERE kt.id = :id")
    public KaryawanTraining getByID(@Param("id") Long id);
    }

