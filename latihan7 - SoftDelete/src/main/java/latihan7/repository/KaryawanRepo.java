package latihan7.repository;


import latihan7.entity.Karyawan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface KaryawanRepo extends PagingAndSortingRepository<Karyawan, Long> {
    @Query("SELECT  k from  Karyawan  k")
    public List<Karyawan> getList();

    @Query("select k from Karyawan k WHERE k.id = :id")
    public Karyawan getByID(@Param("id") Long id);

    Page<Karyawan> findByName(String nama, Pageable pageable);

    Page<Karyawan> findByNameLike(String nama, Pageable pageable);
    Page<Karyawan> findByStatus(Integer status, Pageable pageable);

}
