package latihan7.repository;

import latihan7.entity.KaryawanDetail;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KaryawanDetailRepo extends PagingAndSortingRepository<KaryawanDetail, Long> {
    @Query("SELECT  kd from KaryawanDetail  kd")
    public List<KaryawanDetail> getList();

    @Query("select kd from KaryawanDetail  kd WHERE kd.id = :id")
    public KaryawanDetail getByID(@Param("id") Long id);

}

