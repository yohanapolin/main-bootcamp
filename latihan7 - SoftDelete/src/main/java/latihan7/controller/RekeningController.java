package latihan7.controller;


import latihan7.entity.Rekening;
import latihan7.repository.RekeningRepo;
import latihan7.service.RekeningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path ="rekening")
public class RekeningController {
    @Autowired
    public RekeningRepo rekeningRepo;
    @Autowired
    public RekeningService rekeningService;

    public RekeningController(RekeningRepo rekeningRepo, RekeningService rekeningService) {
        this.rekeningRepo = rekeningRepo;
        this.rekeningService = rekeningService;
    }



    @GetMapping
    public Map getAllRekening() {
        return rekeningService.getAll();
    }

    @GetMapping(path = "{rekeningId}")
    public Map getOneRekening(@PathVariable("rekeningId") Long rekeningId) {
        return rekeningService.getOne(rekeningId);
    }

    @PostMapping
    public Map postRekening(@Valid @RequestBody Rekening rekening) {

        return rekeningService.insert(rekening);
    }

    @PutMapping
    public Map updateRekening(@Valid @RequestBody Rekening rekening) {

        return rekeningService.update(rekening);
    }
    @DeleteMapping("{rekeningId}")
    public ResponseEntity<Map> softDelete(@PathVariable(value = "rekeningId")  Long rekeningId) {
        Map<String, Object> map = new HashMap<>();
        Rekening obj = rekeningRepo.getByID(rekeningId);
        obj.setDeleted_date(new Date());
        rekeningRepo.save(obj);
        map.put("data", "Delete success");
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }

    @GetMapping("/listbytype")
    public ResponseEntity<Page<Rekening>> listBytype(
            @RequestParam() Integer page,
            @RequestParam() Integer size,
            @RequestParam() String type) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Rekening> list = rekeningRepo.findByType(type, pageable);
        return new ResponseEntity<Page<Rekening>>(list, new HttpHeaders(), HttpStatus.OK);
    }

}

