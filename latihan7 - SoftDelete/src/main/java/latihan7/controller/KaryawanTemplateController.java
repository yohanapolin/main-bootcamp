package latihan7.controller;


import latihan7.entity.Karyawan;
import latihan7.service.KaryawanTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path ="template")
public class KaryawanTemplateController {
    @Autowired
    public KaryawanTemplateService service;

    @GetMapping("/list")
    @ResponseBody
    public ResponseEntity<Map> getList() {
        Map c = service.getlist();
        return new ResponseEntity<Map>(c, HttpStatus.OK);
    }

    @DeleteMapping("{karyawanId}")
    public ResponseEntity<Map> softDelete(@PathVariable(value = "karyawanId")  Long karyawanId) {
        Map<String, Object> map = new HashMap<>();
        Map c = service.delete(karyawanId);//service

        map.put("data", "Delete success");
        return new ResponseEntity<Map>(c, HttpStatus.OK);
    }
    @PostMapping("/save")
    public ResponseEntity<Map> save(@Valid @RequestBody Karyawan karyawan) {

        Map map = new HashMap();
        Map obj = service.insert(karyawan);


        map.put("Request =", karyawan);
        map.put("Response =", obj);
        return new ResponseEntity<Map>(obj, HttpStatus.OK);// response
    }
    @PutMapping
    public  ResponseEntity<Map> update(@Valid @RequestBody Karyawan karyawan){
        Map map = new HashMap();
        Map obj = service.update(karyawan);
        map.put("data", obj);
        return new ResponseEntity<Map>(obj, HttpStatus.OK);
    }



}
