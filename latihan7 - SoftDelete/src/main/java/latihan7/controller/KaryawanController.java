package latihan7.controller;

import latihan7.controller.fileupload.UploadFileResponse;
import latihan7.entity.Karyawan;
import latihan7.entity.Training;
import latihan7.repository.KaryawanRepo;
import latihan7.service.FileStorageService;
import latihan7.service.KaryawanService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path ="karyawan")
public class KaryawanController {
    private static final Logger logger = LoggerFactory.getLogger(
            latihan6restapi.controller.KaryawanController.class
    );
    private final FileStorageService fileStorageService;

    @Autowired
    public KaryawanService karyawanService;
    @Autowired
    public KaryawanRepo karyawanRepo;


    public KaryawanController(FileStorageService fileStorageService, KaryawanService karyawanService, KaryawanRepo karyawanRepo) {
        this.fileStorageService = fileStorageService;
        this.karyawanService = karyawanService;
        this.karyawanRepo = karyawanRepo;
    }


    @GetMapping
    public Map getAllKaryawan(){
        return karyawanService.getAll();
    }
    @GetMapping(path = "{karyawanId}")
    public Map getOneKaryawan(@PathVariable("karyawanId") Long karyawanId) {
        return karyawanService.getOne(karyawanId);
    }
    @GetMapping("/listbystatus")
    public ResponseEntity<Page<Karyawan>> listByStatus(
            @RequestParam() Integer page,
            @RequestParam() Integer size,
            @RequestParam() Integer status) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Karyawan> list = karyawanRepo.findByStatus(status, pageable);
        return new ResponseEntity<Page<Karyawan>>(list, new HttpHeaders(), HttpStatus.OK);
    }


    @PostMapping
    public Map postKaryawan(@Valid @RequestBody Karyawan karyawan) {

        return karyawanService.insert(karyawan);
    }

    @PutMapping
    public  ResponseEntity<Map> update(@Valid @RequestBody Karyawan karyawan){
        Map map = new HashMap();
        Map obj = karyawanService.update(karyawan);
        map.put("data", obj);
        return new ResponseEntity<Map>(obj, HttpStatus.OK);
    }
    @RequestMapping(
            value = "/upload",
            method = RequestMethod.POST,
            consumes = {"multipart/form-data", "application/json"}
    )
    public UploadFileResponse uploadFile(@RequestParam() MultipartFile file,
                                         @RequestParam() Long id) throws IOException {
        try {
            fileStorageService.storeFile(file);
            Karyawan obj = karyawanRepo.getByID(id);
            obj.setUploadfile(file.getOriginalFilename());
            karyawanRepo.save(obj);
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace();
            return new UploadFileResponse(
                    file.getOriginalFilename(),
                    null,
                    file.getContentType(),
                    file.getSize(),
                    e.getMessage()
            );
        }

        String fileDownloadUri = ServletUriComponentsBuilder.
                fromCurrentContextPath().
                path("/karyawan/showfile/").
                path(file.getOriginalFilename()).
                toUriString();

        return new UploadFileResponse(
                file.getOriginalFilename(),
                fileDownloadUri,
                file.getContentType(),
                file.getSize(),
                "false"
        );
    }

    @GetMapping("/showfile/{fileName:.+}")
    public ResponseEntity<Resource> showFile(@PathVariable String fileName,
                                             HttpServletRequest request) {
        String contentType = null;

        try {
            contentType = request.getServletContext().getMimeType(
                    fileStorageService.
                            loadFileAsResource(fileName).
                            getFile().
                            getAbsolutePath()
            );
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\""
                                + fileStorageService.loadFileAsResource(fileName).getFilename()
                                + "\""
                ).
                body(fileStorageService.loadFileAsResource(fileName));
    }

    @DeleteMapping("{karyawanId}")
    public ResponseEntity<Map> softDelete(@PathVariable(value = "karyawanId")  Long karyawanId) {
        Map<String, Object> map = new HashMap<>();
        Karyawan obj = karyawanRepo.getByID(karyawanId);
        obj.setDeleted_date(new Date());
        karyawanRepo.save(obj);
        map.put("data", "Delete success");
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id){
        Map map = new HashMap();
        Karyawan obj = karyawanRepo.getByID(id);
        obj.setDeleted_date(new Date());
        karyawanRepo.save(obj);
        map.put("data", "sukses delete");
        return new ResponseEntity<Map>(map,HttpStatus.OK);
    }






}
