package latihan7.controller;


import latihan7.entity.Rekening;
import latihan7.entity.Training;
import latihan7.repository.TrainingRepo;
import latihan7.service.TrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path ="training")
public class TrainingController {
    @Autowired
    public TrainingService trainingService;

    @Autowired
    public TrainingRepo trainingRepo;

    @PostMapping("/save")
    public ResponseEntity<Map> save(@Valid @RequestBody Training training){
        Map map = new HashMap();
        Map obj = trainingService.insert(training);
        map.put("data", obj);
        return new ResponseEntity<Map>(obj, HttpStatus.OK);
    }
    @PutMapping("/update")
    public ResponseEntity<Map> update(@Valid @RequestBody Training training){
        Map map = new HashMap();
        Map obj = trainingService.update(training);
        map.put("data", obj);
        return new ResponseEntity<Map>(obj, HttpStatus.OK);


    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> delete(@PathVariable(value = "id") Long id) {

        Map map = new HashMap();
//        Map c = servis.delete(id);
        Training obj = trainingRepo.getByID(id);
        obj.setDeleted_date(new Date());
        trainingRepo.save(obj);
        map.put("data", "sukses delete");
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }
    @GetMapping("/listbymentor")
    public ResponseEntity<Page<Training>> listByMentor(
            @RequestParam() Integer page,
            @RequestParam() Integer size,
            @RequestParam() String mentor) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Training> list = trainingRepo.findByMentor(mentor, pageable);
        return new ResponseEntity<Page<Training>>(list, new HttpHeaders(), HttpStatus.OK);
    }
}
