package latihan7.controller;

import latihan7.entity.Karyawan;
import latihan7.entity.KaryawanTraining;
import latihan7.repository.TrainingKaryawanRepo;
import latihan7.service.TrainingKaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping(path ="trainer")
public class KaryawanTrainingController {
    @Autowired
    public TrainingKaryawanService trainingKaryawanService;

    @Autowired
    public TrainingKaryawanRepo trainingKaryawanRepo;

    @PostMapping("/save")
    public Map postKaryawanTraining(@Valid @RequestBody KaryawanTraining karyawanTraining) {
        return trainingKaryawanService.insert(karyawanTraining);
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<Map> getlist(){
        Map map = trainingKaryawanService.getData();
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }
    @DeleteMapping("{karyawanTrainingId}")
    public ResponseEntity<Map> softDelete(@PathVariable(value = "karyawanTrainingId")  Long karyawanTrainingId) {
        Map<String, Object> map = new HashMap<>();
        KaryawanTraining obj = trainingKaryawanRepo.getByID(karyawanTrainingId);
        obj.setDeleted_date(new Date());
        trainingKaryawanRepo.save(obj);
        map.put("data", "Delete success");
        return new ResponseEntity<Map>(map, HttpStatus.OK);
    }

    @PutMapping
    public  ResponseEntity<Map> update(@Valid @RequestBody KaryawanTraining karyawanTraining){
        Map map = new HashMap();
        Map obj = trainingKaryawanService.update(karyawanTraining);
        map.put("data", obj);
        return new ResponseEntity<Map>(obj, HttpStatus.OK);
    }
}
