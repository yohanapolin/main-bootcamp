package latihan7.service;

import latihan7.entity.Karyawan;

import java.util.Map;

public interface KaryawanTemplateService {
    public Map  getlist();
    public Map insert(Karyawan karyawan);
    public Map update(Karyawan karyawan);
    public Map delete(Long karyawanId);
}
