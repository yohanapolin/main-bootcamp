package latihan7.service;


import latihan7.entity.Karyawan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

public interface KaryawanService {
    public Map getAll();
    public Map getOne(Long karyawanId);
    public Map insert(Karyawan karyawan);
    public Map update(Karyawan karyawan);
    public Map findByName(String name, Integer page, Integer size);
    Page<Karyawan> findByNameLike (String name, Pageable pageable);
    public Map delete(Long karyawanId);

}
