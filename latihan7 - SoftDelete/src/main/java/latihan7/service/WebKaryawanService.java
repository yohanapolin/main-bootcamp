package latihan7.service;

import latihan7.config.BadResourceException;
import latihan7.config.ResourceNotFoundException;
import latihan7.entity.Karyawan;

import java.util.List;

public interface WebKaryawanService {
    public List<Karyawan> listKaryawanTymeleaf(int pageNumber, int ROW_PER_PAGE);

    public boolean existsByIdTymeleaf(Long karyawanId);

    public Karyawan findByIdTymeleaf(Long karyawanId) throws ResourceNotFoundException;

    public Karyawan saveTymeleaf(Karyawan karyawan) throws BadResourceException;

    public void updateTymeleaf(Karyawan karyawan) throws ResourceNotFoundException, BadResourceException;

    public void deleteByIdTymeleaf(Long karyawanId) throws ResourceNotFoundException;
}
