package latihan7.service;

import latihan7.entity.KaryawanDetail;

import java.util.Map;

public interface KaryawanDetailService {
    public Map getAll();
    public Map getOne(Long karyawanDetailId);
    public Map insert (KaryawanDetail karyawanDetail);
    public Map update(KaryawanDetail karyawanDetail);
    public Map delete(Long karyawanDetailId);

}
