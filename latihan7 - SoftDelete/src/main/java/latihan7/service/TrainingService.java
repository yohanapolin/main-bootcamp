package latihan7.service;

import latihan7.entity.Training;

import java.util.Map;

public interface TrainingService {
    public Map insert(Training training);
    public Map getAll();
    public Map update(Training training);
}
