package latihan7.service;

import latihan7.entity.Rekening;

import java.util.Map;

public interface RekeningTemplataService {
    public Map insert(Rekening rekening, Long karyawanId);// request lempar objek

    public Map update(Rekening rekening, Long karyawanId); //DI objek request

    public Map delete(Rekening rekening);// delete by id

    public Map getData();
}
