package latihan7.service.implement;


import latihan7.entity.Karyawan;
import latihan7.entity.Rekening;
import latihan7.repository.RekeningRepo;
import latihan7.service.RekeningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.*;

@Service
@Transactional
public class RekeningImplement implements RekeningService {

    @Autowired
    public RekeningRepo rekeningRepo;
    @Override
    public Map getAll() {
        List<Rekening> list = new ArrayList<Rekening>();
        Map<String, Object> map = new HashMap<>();
        try {
            list =  rekeningRepo.getList();
            map.put("data", list);
            map.put("status", 200);
            map.put("message", "Get all karyawan success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error get all karyawan");
            return map;
        }
    }

    @Override
    public Map getOne(Long rekeningId) {
        Map<String, Object> map = new HashMap<>();
        try {
            Rekening obj = rekeningRepo.getByID(rekeningId);
            map.put("data", obj);
            map.put("status", 200);
            map.put("message", "Get rekening success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error get Rekening");
            return map;
        }
    }

    @Override
    public Map insert(Rekening rekening) {
        Map<String, java.io.Serializable> map = new HashMap<>();
        try {
            Rekening obj = rekeningRepo.save(rekening);
            map.put("data", obj);
            map.put("status", 200);
            map.put("message", "Insert Rekening success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error insert Rekening");
            return map;
        }
    }

    @Override
    public Map update(Rekening rekening) {
        Map<String, Serializable> map = new HashMap<String, Serializable>();
        try {
            Rekening obj = rekeningRepo.getByID(rekening.getId());
            if (rekening.getName() != null &&
                    rekening.getName().length() > 0 &&
                    !Objects.equals(obj.getName(), rekening.getName())) {
                obj.setName(rekening.getName());
            }
            obj.setName(rekening.getName());

            rekeningRepo.save(obj);
            map.put("data", obj);
            map.put("status", 200);
            map.put("message", "Update karyawan success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error update karyawan");
            return map;
        }
    }

    @Override
    public Map delete(Long rekeningIid) {
        Map map = new HashMap();
        try {
        Rekening obj = rekeningRepo.getByID(rekeningIid);
        if (obj == null){
            map.put("StatusCode", 404);
            map.put("StatusMessage", "data id tidak ditemukan");
            return map;
        }
        rekeningRepo.deleteById(obj.getId());
        map.put("statuscode",200);
        map.put("statusMessage", "delete sukses");
        return map;
    }
        catch (Exception e){
        e.printStackTrace();
        map.put("StatusCode", 500);
        map.put("StatusMessage",e);
        return map;
    }
}
}
