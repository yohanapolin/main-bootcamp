package latihan7.service.implement;

import latihan7.entity.Karyawan;
import latihan7.entity.KaryawanTraining;
import latihan7.entity.Training;
import latihan7.repository.KaryawanRepo;
import latihan7.repository.TrainingKaryawanRepo;
import latihan7.repository.TrainingRepo;
import latihan7.service.TrainingKaryawanService;
import latihan7.utils.MyConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class TrainingKaryawanImplement implements TrainingKaryawanService {
    @Autowired
    public TrainingKaryawanRepo trainingKaryawanRepo;

    @Autowired
    public KaryawanRepo karyawanRepo;
    @Autowired
    public TrainingRepo trainingRepo;

    MyConfig myConfig = new MyConfig();

    public TrainingKaryawanImplement(TrainingKaryawanRepo trainingKaryawanRepo, KaryawanRepo karyawanRepo, TrainingRepo trainingRepo) {
        this.trainingKaryawanRepo = trainingKaryawanRepo;
        this.karyawanRepo = karyawanRepo;
        this.trainingRepo = trainingRepo;
    }

    @Override
    public Map insert(KaryawanTraining karyawanTraining) {
       Map map = new HashMap();

       try {
           Karyawan karyawan = karyawanRepo.getByID(karyawanTraining.getKaryawan().getId());
           Training training = trainingRepo.getByID(karyawanTraining.getTraining().getId());
           karyawanTraining.setKaryawan(karyawan);
           karyawanTraining.setTraining(training);
           KaryawanTraining obj = trainingKaryawanRepo.save(karyawanTraining);

           map.put(myConfig.getData(), obj);
           map.put(myConfig.getStatusCode(), myConfig.getStatusSuksesCode());
           map.put(myConfig.getStatusMessage(), myConfig.getStatusSuksesDesc());
           return map;
       }
       catch (Exception e){
           e.printStackTrace();
           map.put(myConfig.getStatusCode(),myConfig.getStatusFailCode());
           map.put(myConfig.getStatusMessage(), e);
           return map;
       }
    }

    @Override
    public Map getData() {
      List<KaryawanTraining> list = new ArrayList<KaryawanTraining>();
        Map map = new HashMap();
        try {
            list = trainingKaryawanRepo.getList();
            map.put("data", list);
            map.put("status", 200);
            map.put("message", "Get all karyawan success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error get all karyawan");
            return map;
        }
    }

    @Override
    public Map update(KaryawanTraining karyawanTraining) {
        Map map = new HashMap();
        try {
            KaryawanTraining obj = trainingKaryawanRepo.getByID(karyawanTraining.getId());
            if (obj == null) {
                map.put("StatusCode", 404);
                map.put("Status Messaga", "Data id belum ditemukan ");
                return map;
            } else {
                if (karyawanTraining.getKaryawan() == null) {
                    map.put(myConfig.getStatusCode(), myConfig.getStatusFailCode());
                    map.put(myConfig.getStatusMessage(), myConfig.getStatusNotfoundCode());
                    return map;
                }
                Karyawan karyawan = karyawanRepo.getByID(karyawanTraining.getKaryawan().getId());
                if (karyawanTraining.getTraining() == null) {
                    map.put(myConfig.getStatusCode(), myConfig.getStatusNotfoundCode());
                    map.put(myConfig.getStatusMessage(), myConfig.getStatusNotfoundDesc());
                    return map;
                }
                Training training = trainingRepo.getByID(karyawanTraining.getTraining().getId());
                obj.setTraining(training);
                obj.setKaryawan(karyawan);
                obj.setDate_of_training(karyawanTraining.getDate_of_training());
                trainingKaryawanRepo.save(obj);
                map.put("data", obj);
                map.put("statusCode", "200");
                map.put("statusMessage", "Update Sukses");
                return map;
            }


        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map delete(Long karyawanTrainingId) {
        Map map = new HashMap();
        try {
            KaryawanTraining obj = trainingKaryawanRepo.getByID(karyawanTrainingId);
            if (obj == null){
                map.put("StatusCode", 404);
                map.put("StatusMessage", "data id tidak ditemukan");
                return map;
            }
            trainingKaryawanRepo.deleteById(obj.getId());
            map.put("statuscode",200);
            map.put("statusMessage", "delete sukses");
            return map;
        }
        catch (Exception e){
            e.printStackTrace();
            map.put("StatusCode", 500);
            map.put("StatusMessage",e);
            return map;
        }
    }


}
