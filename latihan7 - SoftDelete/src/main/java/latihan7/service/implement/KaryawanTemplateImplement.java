package latihan7.service.implement;

import latihan7.entity.Karyawan;
import latihan7.service.KaryawanTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service //service wajib
@Transactional // opsional :
public class KaryawanTemplateImplement implements KaryawanTemplateService {
    @Autowired
    private RestTemplateBuilder restTemplateBuilder;


    @Override
    public Map getlist() {
        List<Karyawan> list = new ArrayList<Karyawan>();
        Map map = new HashMap();

        try {
            String url = "http://127.0.0.1:8099/karyawan/";
            ResponseEntity<Map> result = restTemplateBuilder.build().
                    exchange(url,
                            HttpMethod.GET, null, new ParameterizedTypeReference<Map>() {
                            });


            map.put("data", result.getBody());//data
            map.put("statusCode", 200);
            map.put("statusMessage", "Get Sukses");
            return map;//success

        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;// eror

        }

    }

    @Override
    public Map insert(Karyawan karyawan) {
        Map map = new HashMap();
        try {
            String url = "http://127.0.0.1:8099/karyawan/";

            ResponseEntity<Map> result = restTemplateBuilder.build().postForEntity(url, karyawan, Map.class);


            map.put("data", result.getBody());
            map.put("statusCode", "200");
            map.put("statusMessage", "Sukses");
            return map;// respon

        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }

    }

    @Override
    public Map update(Karyawan karyawan) {
        Map map = new HashMap();

        try {
            String url = "http://127.0.0.1:8099/karyawan";

            HttpEntity<Karyawan> req = new HttpEntity<>(karyawan);
            ResponseEntity<Map> result = restTemplateBuilder.build().exchange(url, HttpMethod.PUT, req, new
                    ParameterizedTypeReference<Map>() {
                    });

            map.put("data", result.getBody());
            map.put("statusCode", "200");
            map.put("statusMessage", "Sukses");
            return map;// respon
        }
         catch (Exception e) {
                e.printStackTrace();
                map.put("statusCode", "500");
                map.put("statusMessage", e);
                return map;
            }
    }

    @Override
    public Map delete(Long karyawanId) {
        Map map = new HashMap();

        try {
            String url = "http://127.0.0.1:8099/karyawan/" + karyawanId;
//            ResponseEntity<Map> result = restTemplateBuilder.build().delete(url, karyawan, Map.class);

            ResponseEntity<Map> result = restTemplateBuilder.build().
                    exchange(url,
                            HttpMethod.DELETE, null, new ParameterizedTypeReference<Map>() {});

            map.put("data", result.getBody());
            map.put("statusCode", "200");
            map.put("statusMessage", "Sukses");
            return map;// respon

        } catch (Exception e) {
            e.printStackTrace();
            map.put("StatusCode", 500);
            map.put("StatusMessage",e);
            return map;
        }
    }
}
