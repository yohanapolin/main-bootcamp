package latihan7.service.implement;

import latihan7.config.BadResourceException;
import latihan7.config.ResourceNotFoundException;
import latihan7.entity.Karyawan;
import latihan7.entity.KaryawanDetail;
import latihan7.repository.KaryawanDetailRepo;
import latihan7.repository.KaryawanRepo;
import latihan7.service.WebKaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class WebKaryawanImplement implements WebKaryawanService {
    @Autowired
    public KaryawanRepo  karyawanRepository;
    @Autowired
    public KaryawanDetailRepo detailKaryawanRepository;

    public WebKaryawanImplement(KaryawanRepo karyawanRepository, KaryawanDetailRepo detailKaryawanRepository) {
        this.karyawanRepository = karyawanRepository;
        this.detailKaryawanRepository = detailKaryawanRepository;
    }


    @Override
    public List<Karyawan> listKaryawanTymeleaf(
            int pageNumber,
            int ROW_PER_PAGE
    ) {
        List<Karyawan> karyawan = new ArrayList<>();
        Pageable sortedByIdAsc = PageRequest.of(pageNumber - 1, ROW_PER_PAGE,
                Sort.by("id").ascending());
        karyawanRepository.findAll(sortedByIdAsc).forEach(karyawan::add);
        return karyawan;
    }

    @Override
    public boolean existsByIdTymeleaf(Long karyawanId) {
        return karyawanRepository.existsById(karyawanId);
    }

    @Override
    public Karyawan findByIdTymeleaf(
            Long karyawanId
    ) throws ResourceNotFoundException {
        Karyawan karyawan = karyawanRepository.findById(karyawanId).orElse(null);
        if (karyawan == null) {
            throw new ResourceNotFoundException(
                    "Cannot find karyawan with id: " + karyawanId
            );
        } else return karyawan;
    }

    @Override
    public Karyawan saveTymeleaf(
            Karyawan karyawan
    ) throws BadResourceException {
        if (!StringUtils.isEmpty(karyawan.getName())) {
            KaryawanDetail detailKaryawan = detailKaryawanRepository.save(
                    karyawan.getKaryawanDetail()
            );
            Karyawan temp = karyawanRepository.save(karyawan);
            detailKaryawan.setKaryawan(temp);
            detailKaryawanRepository.save(detailKaryawan);
            return temp;
        } else {
            BadResourceException exc = new BadResourceException(
                    "Failed to save karyawan"
            );
            exc.addErrorMessage("Karyawan is null or empty");
            throw exc;
        }
    }

    @Override
    public void updateTymeleaf(
            Karyawan karyawan
    ) throws ResourceNotFoundException, BadResourceException {
        if (!StringUtils.isEmpty(karyawan.getName())) {
            if (!existsByIdTymeleaf(karyawan.getId())) {
                throw new ResourceNotFoundException(
                        "Cannot find karyawan with id: " + karyawan.getId()
                );
            }
            Karyawan temp1 = karyawanRepository.getByID(karyawan.getId());
            if (karyawan.getName() != null &&
                    karyawan.getName().length() > 0 &&
                    !Objects.equals(temp1.getName(), karyawan.getName())) {
                temp1.setName(karyawan.getName());
            }
            if (karyawan.getGender() != null &&
                    karyawan.getGender().length() > 0 &&
                    !Objects.equals(temp1.getGender(), karyawan.getGender())) {
                temp1.setGender(karyawan.getGender());
            }
            if (karyawan.getDob() != null &&
                    !Objects.equals(temp1.getDob(), karyawan.getDob())) {
                temp1.setDob(karyawan.getDob());
            }
            if (karyawan.getAddress() != null &&
                    karyawan.getAddress().length() > 0 &&
                    !Objects.equals(temp1.getAddress(), karyawan.getAddress())) {
                temp1.setAddress(karyawan.getAddress());
            }
            if (karyawan.getStatus() != null &&
                    !Objects.equals(temp1.getStatus(), karyawan.getStatus())) {
                temp1.setStatus(karyawan.getStatus());
            }
            KaryawanDetail temp2 = detailKaryawanRepository.getByID(temp1.getKaryawanDetail().getId());
            if (karyawan.getKaryawanDetail().getNik() != null &&
                    karyawan.getKaryawanDetail().getNik().length() > 0 &&
                    !Objects.equals(temp2.getNik(), karyawan.getKaryawanDetail().getNik())) {
                temp2.setNik(karyawan.getKaryawanDetail().getNik());
            }
            if (karyawan.getKaryawanDetail().getNpwp() != null &&
                    karyawan.getKaryawanDetail().getNpwp().length() > 0 &&
                    !Objects.equals(temp2.getNpwp(), karyawan.getKaryawanDetail().getNpwp())) {
                temp2.setNpwp(karyawan.getKaryawanDetail().getNpwp());
            }
            karyawanRepository.save(temp1);
            detailKaryawanRepository.save(temp2);
        } else {
            BadResourceException exc = new BadResourceException(
                    "Failed to save karyawan"
            );
            exc.addErrorMessage("Karyawan is null or empty");
            throw exc;
        }
    }

    @Override
    public void deleteByIdTymeleaf(
            Long karyawanId
    ) throws ResourceNotFoundException {
        if (!existsByIdTymeleaf(karyawanId)) {
            throw new ResourceNotFoundException(
                    "Cannot find karyawan with id: " + karyawanId
            );
        } else {
            Karyawan obj = karyawanRepository.getByID(karyawanId);
            obj.setDeleted_date(new Date());
            karyawanRepository.save(obj);
        }
    }

}
