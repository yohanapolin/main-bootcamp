package latihan7.service.implement;

import latihan7.entity.Rekening;
import latihan7.service.RekeningTemplataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public class RekeningTemplateImplement implements RekeningTemplataService {
    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Override
    public Map insert(Rekening rekening, Long karyawanId) {
        Map map = new HashMap();
        try {
            String url = "http://127.0.0.1:8099/rekening/"+karyawanId;

            ResponseEntity<Map> result = restTemplateBuilder.build().postForEntity(url, rekening, Map.class);


            map.put("data", result.getBody());
            map.put("statusCode", "200");
            map.put("statusMessage", "Sukses");
            return map;// respon

        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }


    }

    @Override
    public Map update(Rekening rekening, Long karyawanId) {
        return null;
    }

    @Override
    public Map delete(Rekening rekening) {
        return null;
    }

    @Override
    public Map getData() {
        return null;
    }
}
