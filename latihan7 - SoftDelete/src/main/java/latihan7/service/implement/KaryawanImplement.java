package latihan7.service.implement;

import latihan7.entity.Karyawan;
import latihan7.entity.KaryawanDetail;
import latihan7.repository.KaryawanDetailRepo;
import latihan7.repository.KaryawanRepo;
import latihan7.service.KaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
@Transactional
public class KaryawanImplement implements KaryawanService {

    @Autowired
    public KaryawanRepo karyawanRepo;

    @Autowired
    public KaryawanDetailRepo karyawanDetailRepo;

    @Override
    public Map getAll() {
        List<Karyawan> list = new ArrayList<Karyawan>();
        Map map = new HashMap();
        try {
            list = karyawanRepo.getList();
            map.put("data", list);
            map.put("status", 200);
            map.put("message", "Get all karyawan success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error get all karyawan");
            return map;
        }
    }

    @Override
    public Map getOne(Long karyawanId) {
        Map<String, Object> map = new HashMap<>();
        try {
            Karyawan obj = karyawanRepo.getByID(karyawanId);
            map.put("data", obj);
            map.put("status", 200);
            map.put("message", "Get karyawan success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error get karyawan");
            return map;
        }
    }

    @Override
    public Map insert(Karyawan karyawan) {
        Map map = new HashMap();
        try {
            KaryawanDetail karyawanDetail = karyawanDetailRepo.save(
                    karyawan.getKaryawanDetail()
            );

            Karyawan obj = karyawanRepo.save(karyawan);
            map.put("data", obj);
            map.put("status", 200);
            map.put("message", "Insert karyawan success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error insert karyawan");
            return map;
        }
    }

    @Override
    public Map update(Karyawan karyawan) {
        Map map = new HashMap();
        try {
            Karyawan obj = karyawanRepo.getByID(karyawan.getId());
            if(obj == null) {
                map.put("StatusCode", 404);
                map.put("Status Messaga", "Data id belum ditemukan ");
                return map;
            }
            obj.setGender(karyawan.getGender());
            obj.setAddress(karyawan.getAddress());
            obj.setStatus(karyawan.getStatus());
            obj.setUploadfile(karyawan.getUploadfile());
            karyawanRepo.save(obj);
            map.put("data", obj);
            map.put("status", 200);
            map.put("message", "Update karyawan success");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", 500);
            map.put("error", e);
            map.put("message", "Error update karyawan");
            return map;
        }
    }

    @Override
    public Map findByName(String name, Integer page, Integer size) {
        Map map = new HashMap();
        try {
            Pageable s_data = PageRequest.of(page, size);
            Page<Karyawan> list = karyawanRepo.findByName(name, s_data);
            map.put("data", list);
            map.put("StatusCode", 200);
            map.put("StatusMessage", "Get Success");
            return map;
        }
        catch (Exception e){
            e.fillInStackTrace();
            map.put("StatusCode", 500);
            map.put("StatusMessage", e);
            return map;
        }
    }

    @Override
    public Page<Karyawan> findByNameLike(String name, Pageable pageable) {
        return null;
    }

    @Override
    public Map delete(Long karyawanId) {
        Map map = new HashMap();
        try {
            Karyawan obj = karyawanRepo.getByID(karyawanId);
            if (obj == null){
                map.put("StatusCode", 404);
                map.put("StatusMessage", "data id tidak ditemukan");
                return map;
            }
            karyawanRepo.deleteById(obj.getId());
            map.put("statuscode",200);
            map.put("statusMessage", "delete sukses");
            return map;
        }
        catch (Exception e){
            e.printStackTrace();
            map.put("StatusCode", 500);
            map.put("StatusMessage",e);
            return map;
        }
    }
}
