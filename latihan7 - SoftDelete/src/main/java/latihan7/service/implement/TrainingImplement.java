package latihan7.service.implement;

import latihan7.entity.Training;
import latihan7.repository.KaryawanRepo;
import latihan7.repository.TrainingRepo;
import latihan7.service.TrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class TrainingImplement implements TrainingService {

    @Autowired
    public TrainingRepo trainingRepo;
    @Autowired
    public KaryawanRepo karyawanRepo;

    @Override
    public Map insert(Training training) {
        Map map = new HashMap();
        try {
            Training obj = trainingRepo.save(training);
            map.put("data", obj);
            map.put("statusCode", 200);
            map.put("StatusMessage", "sukses");
            return map;
        }
        catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", 500);
            map.put("Status Message", e);
            return map;
        }
    }



    @Override
    public Map getAll() {
        List<Training>  list = new ArrayList<Training>();
        Map map = new HashMap();
        try {
            list = trainingRepo.getList();
            map.put("data", list);//data
            map.put("statusCode", 200);
            map.put("statusMessage", "Get Sukses");
            return map;//success

        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;// eror
        }
    }

    @Override
    public Map update(Training training) {
        Map map = new HashMap();
        try {
            Training obj = trainingRepo.getByID(training.getId());
            if(obj == null) {
                map.put("statusCode", 404);
                map.put("statusMessage", "not found");
                return  map;
            }
            obj.setThema(training.getThema());
            obj.setMentor(training.getMentor());
            trainingRepo.save(obj);
            map.put("data", obj);
            map.put("statusCode", "200");
            map.put("statusMessage", "Update Sukses");
            return map;

        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }
}
