package latihan7.service;

import latihan7.entity.KaryawanTraining;

import java.util.Map;

public interface TrainingKaryawanService  {
    public Map insert(KaryawanTraining karyawanTraining);
    public Map getData();
    public Map update(KaryawanTraining karyawanTraining);
    public Map delete(Long karyawanTrainingId);
}
