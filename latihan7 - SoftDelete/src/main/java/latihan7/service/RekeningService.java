package latihan7.service;

import latihan7.entity.Rekening;

import java.util.Map;

public interface RekeningService {
    public Map getAll();
    public Map getOne(Long rekeningId);
    public Map insert(Rekening rekening);
    public Map update(Rekening rekening);
    public Map delete(Long rekeningId);
}
