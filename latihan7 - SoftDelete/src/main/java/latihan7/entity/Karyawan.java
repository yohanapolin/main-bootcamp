package latihan7.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Data
@Table(name ="karyawan")
@Where(clause = "deleted_date is null")
public class Karyawan extends AbstractDate implements Serializable {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    private Long id;
    private String name;
    private String gender;
    private Date dob;
    private String address;
    private Integer status;
    private String uploadfile;


    @OneToOne(mappedBy = "karyawan")
    private KaryawanDetail karyawanDetail;

    @JsonIgnore
    @OneToMany(mappedBy = "karyawan",  fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Rekening> rekenings;


    @JsonIgnore
    @OneToMany(mappedBy = "karyawan")
    private List<KaryawanTraining> karyawanTraining;



}
