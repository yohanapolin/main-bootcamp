package latihan7.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table( name ="karyawan_detail")
@Where(clause = "deleted_date is null")
public class KaryawanDetail extends AbstractDate implements Serializable {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    private  Long id;
    private String nik;
    private String npwp;
    @JsonIgnore
    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "karyawan_id")
    private Karyawan karyawan;


}
