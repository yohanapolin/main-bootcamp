package latihan7.entity;


import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "karyawan_training")
@Where(clause = "deleted_date is null")
public class KaryawanTraining extends AbstractDate implements Serializable {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    private Long id;

    @ManyToOne
    @JoinColumn(name = "training_id")
    private  Training training;
    private Date date_of_training;

    @ManyToOne
    @JoinColumn(name = "karyawan_id")
     private  Karyawan karyawan;
}
