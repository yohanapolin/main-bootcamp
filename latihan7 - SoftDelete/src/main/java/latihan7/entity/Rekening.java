package latihan7.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "rekening")
@Where(clause = "deleted_date is null")
public class Rekening extends AbstractDate implements Serializable {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    private  Long id;
    private String name;
    private String type;
    private String nomor;



    @ManyToOne
    @JoinColumn(name = "karyawan_id")
    @JsonIgnore
    private Karyawan karyawan;


}
