package latihan7.entity;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@Document(collection = "karyawan")
public class Karyawan  {
    @Id
    private String id;
    private String name;

    private String address;

    public Karyawan(String name, String address) {
        this.name = name;
        this.address = address;
    }

    @Override
    public String toString() {
        return "Karyawan{" +
                "id='" + id + '\'' +
                ", nama='" + name + '\'' +
                ", alamat='" + address + '\'' +
                '}';
    }



}
