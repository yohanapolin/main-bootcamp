package latihan7.repository;


import latihan7.entity.Karyawan;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;

public interface KaryawanRepo extends MongoRepository<Karyawan, String> {
    List<Karyawan> findByNameContaining(String name);

}
