package latihan7.controller;

import latihan7.entity.Karyawan;
import latihan7.repository.KaryawanRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/karyawan")
public class KaryawanController {


    private final KaryawanRepo repo;
    @Autowired
    public KaryawanController(KaryawanRepo repo) {
        this.repo = repo;
    }

    @GetMapping()
    public ResponseEntity<List<Karyawan>> getAll(@RequestParam(required = false) String nama) {
        try {
            List<Karyawan> karyawans = new ArrayList<Karyawan>();

            if (nama == null)
                repo.findAll().forEach(karyawans::add);
            else
                repo.findByNameContaining(nama).forEach(karyawans::add);

            if (karyawans.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(karyawans, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/{id}")
    public ResponseEntity<Karyawan> getKaryawanById(@PathVariable("id") String id) {
        Optional<Karyawan> karyawan = repo.findById(id);
        return karyawan.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND)
                );
    }



    @PostMapping("/create")
    public ResponseEntity<Karyawan> create(@RequestBody Karyawan karyawan) {
        try {
            Karyawan obj = repo.save(karyawan);
            return new ResponseEntity<>(obj, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PutMapping("/{id}")
    public ResponseEntity<Karyawan> updateKaryawan(@PathVariable("id") String id, @RequestBody Karyawan karyawan) {
        Optional<Karyawan> karyawant= repo.findById(id);
        if (karyawant.isPresent()) {
            Karyawan karyawan1 = karyawant.get();
            karyawan1.setName(karyawan.getName());
            karyawan1.setAddress(karyawan.getAddress());
            return new ResponseEntity<>(repo.save(karyawan1), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteKaryawan(@PathVariable("id") String id) {
        try {
            repo.deleteById(id);
            String message = "Data berhasil dihapus";
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    }










