package latihan6restapi.entity;

import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;


@Setter
@Entity
@Table(name = "karyawan")
public class Karyawan implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "karyawan_sequence"
    )
    private Long id;
    private String nama;
    private Integer jeniskelamin;
    private LocalDate dob;
    private String alamat;
    private Integer status;

    public Long getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public Integer getJeniskelamin() {
        return jeniskelamin;
    }

    public LocalDate getDob() {
        return dob;
    }

    public String getAlamat() {
        return alamat;
    }

    public Integer getStatus() {
        return status;
    }

    public Karyawan() {
    }

    public Karyawan( String nama, Integer jeniskelamin, LocalDate dob, String alamat, Integer status) {

        this.nama = nama;
        this.jeniskelamin = jeniskelamin;
        this.dob = dob;
        this.alamat = alamat;
        this.status = status;
    }

    @Override
    public String toString() {
        return "Karyawan{" +
                "id : " + id +
                ", nama: ' " + nama + '\'' +
                ", jeniskelamin : " + jeniskelamin +
                ", dob:" + dob +
                ", alamat : '" + alamat + '\'' +
                ", status : '" + status + '\'' +
                '}';
    }
}
